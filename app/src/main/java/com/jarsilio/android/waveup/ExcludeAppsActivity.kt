package com.jarsilio.android.waveup

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jarsilio.android.waveup.adapter.AppListAdapter
import com.jarsilio.android.waveup.model.AppDatabase
import com.jarsilio.android.waveup.model.AppsDao
import com.jarsilio.android.waveup.model.AppsHandler
import com.jarsilio.android.waveup.model.AppsViewModel
import com.jarsilio.android.waveup.model.EmptyRecyclerView

class ExcludeAppsActivity : AppCompatActivity() {
    private val appsHandler: AppsHandler by lazy { AppsHandler(this) }
    private val appsDao: AppsDao by lazy { AppDatabase.getInstance(this).appsDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_exclude_apps)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val excludedAppsRecyclerView = findViewById<EmptyRecyclerView>(R.id.recycler_excluded_apps)
        val emptyView = findViewById<CardView>(R.id.empty_view)
        excludedAppsRecyclerView.setEmptyView(emptyView)
        excludedAppsRecyclerView.layoutManager = LinearLayoutManager(this)
        val excludedAppsListAdapter = AppListAdapter()
        excludedAppsRecyclerView.adapter = excludedAppsListAdapter

        val notExcludedAppsRecyclerView = findViewById<RecyclerView>(R.id.recycler_not_excluded_apps)
        notExcludedAppsRecyclerView.layoutManager = LinearLayoutManager(this)
        val notExcludedAppsListAdapter = AppListAdapter()
        notExcludedAppsRecyclerView.adapter = notExcludedAppsListAdapter

        val viewModel = ViewModelProvider(this).get(AppsViewModel::class.java)
        viewModel.getExcludedApps(appsDao)
            .observe(
                this,
                { list ->
                    excludedAppsListAdapter.submitList(list)
                }
            )
        viewModel.getNotExcludedApps(appsDao).observe(
            this,
            { list ->
                notExcludedAppsListAdapter.submitList(list)
            }
        )

        // This enables inertia while scrolling
        excludedAppsRecyclerView.isNestedScrollingEnabled = false
        notExcludedAppsRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onResume() {
        super.onResume()
        appsHandler.updateAppsDatabase()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}

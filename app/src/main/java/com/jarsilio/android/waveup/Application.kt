package com.jarsilio.android.waveup

import android.content.Context
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.multidex.MultiDexApplication
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import com.jarsilio.android.waveup.receivers.ServiceTogglerReceiver
import timber.log.Timber

@Suppress("unused")
class WaveUpApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))

        Timber.d("Registering ServiceToggler BroadcastReceiver for com.jarsilio.android.waveup.action.WAVEUP_ENABLE")
        val filter = IntentFilter("com.jarsilio.android.waveup.action.WAVEUP_ENABLE")

        val receiver = ServiceTogglerReceiver()
        ContextCompat.registerReceiver(applicationContext, receiver, filter, ContextCompat.RECEIVER_EXPORTED)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }
}

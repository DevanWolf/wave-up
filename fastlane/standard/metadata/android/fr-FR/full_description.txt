WaveUp est une application qui <i>réveille ton téléphone</i> - active ton écran - quand tu <i>survoles</i> le capteur de proximité.

J'ai développé cette application car je voulais éviter d'appuyer sur le bouton "marche" juste pour regarder l'heure - ce que je fais beaucoup sur mon téléphone. Il existe déjà d'autres applications qui font exactement cela - et même plus. J'ai été inspiré par "Gravity Screen On/Off", qui est une <b>super</b> application. Cependant, je suis un grand admirateur des logiciels libres (au code source ouvert) et j'essaie d'installer ce type de logiciels sur mon téléphone lorsque c'est possible. Je n'ai pas trouvé une application libre qui faisait cela donc je l'ai simplement faite moi-même. Si vous êtes intéressés, vous pouvez jetez un œil au code :
https://gitlab.com/juanitobananas/wave-up

Passez simplement votre main au-dessus du capteur de proximité de votre téléphone pour allumer l'écran. Cette configuration s'appelle le <i>"wave mode"</i> et peut être désactivée dans les paramètres afin d'éviter une activation accidentelle de votre écran.

L'écran s'allumera aussi quand vous sortirez votre smartphone de votre poche ou de votre sac à main. Cette configuration s'appelle le <i>"called pocked mode"</i> et peut aussi être désactivée dans les paramètres.

Ces deux modes sont activés par défaut.

Également, votre téléphone se verrouille et l'écran s'éteint si vous couvrez le capteur de proximité pour une seconde (ou une durée déterminée). Cette configuration n'a pas de dénomination spéciale mais peut néanmoins être aussi modifiée dans les paramètres. Elle n'est pas activée par défaut.

Pour ceux.elles qui n'ont jamais entendu parler de capteur de proximité avant : c'est une petite chose qui est quelque part proche de l'endroit où vous mettez votre oreille quand vous parlez au téléphone. Vous ne pouvez pratiquement pas le voir et il est chargé d'indiquer à votre téléphone d'éteindre l'écran quand vous avez une conversation.

<b>Désinstallation</b>

Cette application utilise l'autorisation de l'Administrateur Système. Vous ne pouvez donc pas désinstaller WaveUp "normalement".

Pour la désinstaller, ouvrez la simplement et utilisez le bouton de désinstallation de WaveUp ("Uninstall WaveUp") au bas du menu.

<b>Problèmes/bugs connus</b>

Malheureusement, certains smartphones laissent fonctionner le CPU (processeur) lorsque vous êtes contre le capteur de proximité. Cela s'appelle <i>"wave lock"</i> et vide considérablement la batterie. Ce n'est pas de ma faute et je ne peux rien faire pour changer cela. D'autre téléphones vont se mettre en veille quand l'écran est éteint tandis que le capteur de proximité est toujours actif. Dans ce cas, la perte de batterie est quasiment nulle.

<b>Autorisations requises d'Android</b>

▸ WAKE_LOCK pour allumer l'écran
▸ USES_POLICY_FORCE_LOCK pour verrouiller l'appareil
▸ RECEIVE_BOOT_COMPLETED pour démarrer automatiquement lors du démarrage si ce paramètre est choisi
▸ READ_PHONE_STATE pour suspendre WaveUp pendant un appel

<b>Remarques diverses</b>

C'est la première application Android que j'ai écrite, donc attention !

C'est aussi ma première petite contribution au monde du libre. Enfin !

J'aimerais beaucoup que vous puissiez me donner votre avis, ou contribuer de quelque manière que ce soit !

Merci d'avoir lu !

Le libre déchire !!!

<b>Traductions</b>

Il serait vraiment cool si vous pouviez aider à traduire WaveUp à votre langue (même la version anglaise pourrait probablement être révisée).
Il est disponible pour la traduction comme deux projets sur Transifex: https://www.transifex.com/juanitobananas/waveup/ and https://www.transifex.com/juanitobananas/libcommon/. 

<b>Remerciements</b>

Je remercie tout particulièrement :

Voir : https://gitlab.com/juanitobananas/wave-up/#acknowledgments
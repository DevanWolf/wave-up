WaveUp é unha app que <i>esperta o teu móbil</i> - acende a pantalla - cando <i>pasas a man</i> preto do sensor de proximidade.

Desenvolvín esta app porque non quería ter que premer o botón só para poder ver a hora - o que acontece a miúdo. Existen outras apps que fan o mesmo - e máis cousas. Inspireime en Gravity Screen On/Off, que é unha app <b>moi boa</b>. Porén, son gran fan do software libre e intento instalar software libre no meu móbil sempre que sexa posible. Non atopei ningunha app libre que faga isto así que fíxena eu. Se queres, podes botar un ollo ó código:
https://gitlab.com/juanitobananas/wave-up

Pasa a man por enriba do sensor de proximidade para acender a pantalla. Esto chámase <i>wave mode</i> e pode desactivarse nos axustes da pantalla para evitar acendela sen pretendelo.

Tamén acende a pantalla cando sacas o móbil do peto ou do bolso. Esto chámase <i>pocket mode</i> e tamén se pode desactivar nos axustes da pantalla.

Os dous modos están activados por omisión.

Tamén bloquea o teu móbil e apaga a pantalla se cobres o sensor de proximidade durante un segundo (ou o tempo indicado). Esto non ten un nome especial pero tamén se pode cambiar nos axustes da pantalla. Non está activado por omisión.

Para aquelas persoas que nunca escoitasen acerca do sensor de proximidade: é unha cousiña que está preto de onde pos a orella cando falas polo teléfono. Case non se ve e é responsable de dicirlle ó móbil que apague a pantalla cando estás cunha chamada.

<b>Desinstalar</b>

Esta app usa permiso de Administración do Dispositivo. Así que non podes desinstalar WaveUp de xeito 'normal'.

Para desinstalala, ábrea e usa o botón 'Desinstalar WaveUp' embaixo no menú.

<b>Problemas coñecidos</b>

Desafortunadamente, algúns móbiles deixan a CPU acendida mentras controlan o sensor de proximidade. Esto chámase <i>wake lock</i> e causa un consumo importante de batería. Non é culpa miña e non podo facer nada ó respecto. Outros móbiles "adormecen" cando se apaga a pantalla pero seguen escoitando o sensor. Neste caso, o uso da batería é prácticamente inexistente.

<b>Permisos requeridos:</b>

▸ WAKE_LOCK para acender a pantalla
▸ USES_POLICY_FORCE_LOCK para bloquear o dispositivo
▸ RECEIVE_BOOT_COMPLETED para iniciarse tras o boot se activo
▸ READ_PHONE_STATE para suspender WaveUp durante as chamadas

<b>Notas</b>

Esta é a primeira app Android que escribo, avísovos!

Tamén é a miña primeira colaboración ó software de código aberto. Ao fin!

Agradezo calquera opinión ou suxestión así como contribucións ó proxecto!

Grazas por ler!

O código aberto mola!!

<b>Traducións</b>

Estaría fantástico se puideses axudar a traducir WaveUp ó teu idioma (probablemente a versión inglesa pode ser mellorada).
Está dispoñible como dous proxectos a traducir en Transifex: https://www.transifex.com/juanitobananas/waveup/ and https://www.transifex.com/juanitobananas/libcommon/.

<b>Recoñecemento</b>

Agradecemento especial para:

Podes ler: https://gitlab.com/juanitobananas/wave-up/#acknowledgments
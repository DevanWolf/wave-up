New in 3.2.18
★ Upgrade some dependencies.
★ Remove ACRA (crash reporting).

New in 3.2.17
★ Remove 'Excluded apps' option from Google Play store versions. F-Droid ones remain fully functional. I'm sorry, but Google doesn't allow WaveUp to read list of installed apps, which is necessary for this.
★ Update German and Russian translations.
★ Add bluetooth permission request for Android 14 and above (needed to know if a headset is connected during a call).

New in 3.2.16
★ Upgrade a bunch of dependencies.
★ Android 13 and 14 compatibility
★ Update Chinese (Simplified) and Turkish translations.

New in 3.2.15
★ Update Chinese (Simplified) translation.
★ Upgrade a bunch of dependencies.

New in 3.2.14
★ Fix another stupid bug (QuickSettings Tile not working) introduced in 3.2.12.
